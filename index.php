<?php
//http://moodle.cpilosenlaces.com/mod/resource/view.php?id=11513

require_once __DIR__.'/vendor/autoload.php';

$app = new Silex\Application();
$app['debug'] = true;	//para que nos muestre errores

// código de los controladores

// http://localhost/~jose/workspace-aptana/tutorialsilex/index.php/hola/pepe
$app->get('/hola/{nombre}', function($nombre) use($app) { //si recibes 'hola', lanza una funcion
	return '<h1>Hola <strong>' .$app->escape($nombre).'</strong></h1>';
});





$app->run();

?>