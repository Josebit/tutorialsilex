Instalación
===========

Abre tu navegador y pon la siguiente dirección

::

	https://Josebit@bitbucket.org/Josebit/tutorialsilex.git


Verás un link a descargar en la zona derecha de la pagina web.

Te descargas la carpeta, la renombras a 'tutorialsilex' y la pones en tu servidor LAMP o XAMP (o el que utilices) le das permisos de lectura y ejecución a 'otros' 



Instalar motor de plantillas TWIG
=================================

Desde Terminal:

- Descargar 'composer' (que es un instalador de paquetes con todas sus dependencias):

::

	cd /ruta/de/miproyecto/
	curl -s getcomposer.org/installer | php -d detect_unicode=Off     (o esta línea -en Mac-)
	curl -s http://getcomposer.org/installer | php				  (o esta otra línea -en Ubuntu-)
	
- Crear un archivo llamado 'composer.json' en /ruta/de/miproyecto/  con el siguiente contenido:

::

	{
		"require": {
			"silex/silex": "1.0.*@dev"
		}
	}
	
	
- Ahora que ya hemos descargado composer, lo instalamos:

::
	
	php composer.phar install




Descargarse la base de datos
============================

En descargas podrás descargarte el archivo 'tutorialsilex.sqlite' para meterlo en /ruta/de/miproyecto/


Desde tu navegador
==================

Para ver si esta bien instalado

::

	http://localhost:8888/workspace-aptana/tutorialsilex/index.php/hola/pepe








